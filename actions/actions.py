# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from rasa_sdk.types import DomainDict

import requests
import json


class RandomDogImage(Action):
    '''
    This action makes a request to gets a dog picture from the Dog Api.
    The result of the request is json composed of the state of the request and the image.
    The breed of the dog is saved in the last_breed slot.
    '''

    def name(self) -> Text:
        return "actions.RandomDogImage"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        res = requests.get('https://dog.ceo/api/breeds/image/random')
        response = json.loads(res.text)
        L = []
        if response['status'] == "success":
            breed = response["message"].split('/')[-2].split('-')[0]
            dispatcher.utter_message(text="Here is a dog!")
            dispatcher.utter_message(image=response['message'])
            L.append(SlotSet("last_breed", breed))
        else:
            dispatcher.utter_message(text="Sorry an error has occured!")
            L.append(SlotSet("last_breed", ""))
        return L


class ValidateDogForm(FormValidationAction):
    '''
    The aim of this form action is to check the values of the following slots : breed and sub_breed.
    To verify the values we request the list of breeds of the Dog API.
    '''
    def name(self) -> Text:
        return "validate_dog_form"

    @staticmethod
    def breed_db() -> List[Text]:
        res = requests.get('https://dog.ceo/api/breeds/list/all')
        response = json.loads(res.text)
        return response["message"].keys() #List of available breeds

    @staticmethod
    def sub_breed_db(breed: Text) -> List[Text]:
        res = requests.get('https://dog.ceo/api/breeds/list/all')
        response = json.loads(res.text)
        return response["message"][str(breed)] #List of available sub-breeds

    def validate_breed(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        print("validating breed")
        if slot_value.lower() in self.breed_db():
            return {"breed": slot_value}
        else:
            return {"breed": None}

    def validate_sub_breed(
            self,
            slot_value: Any,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: DomainDict,
    ) -> Dict[Text, Any]:
        print("validating sub-breed")
        sub_breed_list = self.sub_breed_db(tracker.get_slot('breed'))
        if slot_value.lower() in sub_breed_list:
            return {"sub_breed": slot_value}

        elif not sub_breed_list:
            return {"sub_breed": ''}
        else:
            return {"sub_breed": None}


class NewPicture(Action):
    '''
    This action makes a request to gets a dog picture from the Dog Api.
    The result of the request is json composed of the state of the request and the image.
    The breed of the dog is saved in the last_breed slot.
    '''
    def name(self) -> Text:
        return "actions.NewPicture"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        res = requests.get('https://dog.ceo/api/breeds/image/random')
        response = json.loads(res.text)
        L = []
        if response['status'] == "success":
            breed = response["message"].split('/')[-2].split('-')[0]
            dispatcher.utter_message(text="We hope you prefer this one.")
            dispatcher.utter_message(image=response['message'])
            L.append(SlotSet("last_breed", breed))
        else:
            dispatcher.utter_message(text="Sorry an error has occured!")
            L.append(SlotSet("last_breed", ""))
        return L


class AskDogBreed(Action):
    '''
    This action indicates the breed of the last dog seen by the user thanks to the value of the slot last_breed.
    '''
    def name(self) -> Text:
        return "actions.AskDogBreed"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        breed = tracker.get_slot('last_breed')
        if breed != "":
            dispatcher.utter_message(text="The last dog was a " + breed + ".")
        return []


class GetListSubBreed(Action):
    '''
    This action lists all the subbreeds of a specific breed and displays a picture for each.
    A request is done to obtain the list of subbreeds.
    '''
    def name(self) -> Text:
        return "actions.GetListSubBreed"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        breed = tracker.get_slot('breed')
        if breed != "":
            res = requests.get('https://dog.ceo/api/breed/' + breed + '/list')
            response = json.loads(res.text)
            if response["message"] == []:
                dispatcher.utter_message(text="There are no sub-breeds of " + breed + ".")
            else:
                for subbreed in response["message"]:
                    sub = ""
                    while sub != subbreed:
                        print(sub, subbreed)
                        res = requests.get('https://dog.ceo/api/breed/' + breed + '/images/random')
                        resp = json.loads(res.text)
                        sub = resp["message"].split('/')[-2].split('-')[-1]
                    dispatcher.utter_message(text="There is the " + subbreed + " sub-breed.")
                    dispatcher.utter_message(image=resp['message'])
        return []


class RandomDogBreedImage(Action):
    '''
    This actions displays the picture of a specific breed.
    Requests to the Dog API make it possible to get a picture of a breed.
    '''
    def name(self) -> Text:
        return "actions.RandomDogBreedImage"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        breed = tracker.get_slot('breed')
        sub_breed = tracker.get_slot('sub_breed')
        if breed is None:
            return []
        elif sub_breed == '':
            res = requests.get(f'https://dog.ceo/api/breed/{breed}/images/random')
            response = json.loads(res.text)
            if response['status'] == "success":
                dispatcher.utter_message(text=f"Here is a {breed}!")
                dispatcher.utter_message(image=response['message'])
            else:
                dispatcher.utter_message(text="Sorry an error has occured!")
        else:
            res = requests.get(f'https://dog.ceo/api/breed/{breed}/{sub_breed}/images/random')
            response = json.loads(res.text)
            if response['status'] == "success":
                dispatcher.utter_message(text=f"Here is a {sub_breed} {breed}!")
                dispatcher.utter_message(image=response['message'])
            else:
                dispatcher.utter_message(text="Sorry an error has occured!")
        return []
